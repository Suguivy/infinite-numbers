#include "infint.h"
#include "stdio.h"

int main()
{
    infint *n = make_zero_infint();
    while (1) {
        add_to_infint_uint(n, 0xffffffffu);
        print_infint(n);
        printf("\n");
    }
    printf("\n");
    add_to_infint_uint(n, 0x00000001u);
    print_infint(n);
    printf("\n");
    destroy_infint(n);
    return 0;
}
