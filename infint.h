#ifndef INFINT_H
#define INFINT_H

typedef struct sinfint {
    unsigned int digit;
    struct sinfint *next;
} infint;

infint *make_zero_infint();
//infint make_infint(char *value);
//void print_infint(infint *num);
//void add_to_infint(infint *a, infint *b);
void add_to_infint_uint(infint *num, unsigned int uint_num);
void print_infint(infint* num);
void destroy_infint(infint* num);
#endif // INFINT_H
