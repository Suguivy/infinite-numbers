#include "infint.h"
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

infint *make_zero_infint()
{
    infint *num = malloc(sizeof(infint));
    num->digit = 0u;
    num->next = NULL;
    return num;
}

void add_to_infint_uint(infint *num, unsigned int uint_num)
{
    unsigned int remaining = UINT_MAX - num->digit;

    if (uint_num > remaining) {
        if (num->next == NULL) {
            num->next = malloc(sizeof(infint));
            num->next->digit = 0;
            num->next->next = NULL;
        }
        unsigned int overflow = uint_num - remaining - 1;
        num->digit = overflow;
        add_to_infint_uint(num->next, 1u);
    } else {
        num->digit += uint_num;
    }
}

void print_infint(infint *num)
{
    if (num->next != NULL) {
        print_infint(num->next);
        printf("%08x ", num->digit);
    } else {
        printf("0x%x", num->digit);
    }
}

void destroy_infint(infint* num)
{
    if (num->next != NULL) {
       destroy_infint(num->next);
    }
    free(num);
}
